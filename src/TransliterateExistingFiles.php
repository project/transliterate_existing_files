<?php

declare(strict_types = 1);

namespace Drupal\transliterate_existing_files;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Transliterates existing files and create redirects for the ones that changed.
 */
final class TransliterateExistingFiles {

  use StringTranslationTrait;

  /**
   * Constructs a TransliterateExistingFiles object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly MessengerInterface $messenger,
  ) {}

  /**
   * Batch transliterate existing files.
   */
  public function batchTransliterate($options = ['dry-run' => FALSE]): void {
    $fileStorage = $this->entityTypeManager->getStorage('file');
    $fids = $fileStorage
      ->getQuery()
      ->accessCheck(FALSE)
      ->execute();

    if (empty($fids)) {
      $this->messenger->addMessage($this->t('No files yet.'));
      return;
    }

    $batchOperations = [];
    foreach ($fids as $fid) {
      $batchOperations[] = [
        self::class . '::transliterateFile',
        [$fid, $options],
      ];
    }

    $batch = [
      'operations' => $batchOperations,
      'finished' => self::class . '::batchFinished',
      'title' => $this->t('Transliterate existing files.'),
      'init_message' => $this->t('Transliterate existing files starting.'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('Transliterate existing files starting has encountered an error.'),
    ];

    batch_set($batch);
    if (PHP_SAPI === 'cli') {
      drush_backend_batch_process();
    }
  }

  /**
   * Batch operation to transliterate a file.
   */
  public static function transliterateFile($fid, $options, &$context): void {
    $fileStorage = \Drupal::entityTypeManager()->getStorage('file');
    $file = $fileStorage->load($fid);
    $filename = $file->getFilename();

    if (!$filename) {
      return;
    }

    // @todo add option to include / exclude file extensions.
    //   $extension = pathinfo($filename, PATHINFO_EXTENSION);
    //    $excludedExtensions = ['jpeg', 'jpg', 'png'];
    //    if (in_array($extension, $excludedExtensions)) {
    //      return;
    //    }
    $sanitizedFilename = self::sanitizeFilename($filename);
    if ($filename === $sanitizedFilename) {
      return;
    }

    $fileUri = $file->getFileUri();
    $sanitizedUri = str_replace($filename, $sanitizedFilename, $fileUri);

    // @todo convert into a value object.
    $context['results'][] = [
      'fid' => $fid,
      'original_filename' => $filename,
      'sanitized_filename' => $sanitizedFilename,
      'original_uri' => $fileUri,
      'sanitized_uri' => $sanitizedUri,
    ];
    $context['message'] = t('Running transliterate on file id "@id".',
      ['@id' => $fid]
    );

    if (!$options['dry-run']) {
      $toProcess = TRUE;
      // There can be a case of a file that will result
      // in the same filename after transliteration.
      if (file_exists($sanitizedUri)) {
        $toProcess = FALSE;
        $errorMessage = t('File already exists for @fid: @uri', ['@fid' => $fid, '@uri' => $sanitizedUri]);
        \Drupal::logger('transliterate_existing_files')->error($errorMessage);
        \Drupal::messenger()->addError($errorMessage);
      }
      if (!rename($fileUri, $sanitizedUri)) {
        $toProcess = FALSE;
        $errorMessage = t('Could not rename file with @fid to: @uri', ['@fid' => $fid, '@uri' => $sanitizedUri]);
        \Drupal::logger('transliterate_existing_files')->error($errorMessage);
        \Drupal::messenger()->addError($errorMessage);
      }

      if ($toProcess) {
        // Save the file.
        $file->setFileUri($sanitizedUri);
        $file->setFilename($sanitizedFilename);
        $file->save();

        // Optionally create the redirect.
        $isRedirectEnabled = \Drupal::moduleHandler()->moduleExists('redirect');
        $createRedirect = $options['redirect'];
        if (!$isRedirectEnabled && $createRedirect) {
          \Drupal::messenger()->addWarning('redirect option is ignored as the Redirect module is not enabled.');
        }

        if ($isRedirectEnabled && $createRedirect) {
          $urlGenerator = \Drupal::service('file_url_generator');
          $fileUrl = $urlGenerator->generate($fileUri)->toString();
          // Redirect needs that.
          if (str_starts_with($fileUrl, '/')) {
            $fileUrl = ltrim($fileUrl, '/');
          }
          $sanitizedUrl = $urlGenerator->generate($sanitizedUri)->toString();
          $redirectStorage = \Drupal::entityTypeManager()->getStorage('redirect');
          $redirectStorage->create([
            'redirect_source' => $fileUrl,
            'redirect_redirect' => 'internal:' . $sanitizedUrl,
            'language' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
            'status_code' => '301',
          ])->save();
        }
      }
    }
  }

  /**
   * Batch finished callback.
   */
  public static function batchFinished(bool $success, array $results, array $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      if (!empty($results)) {
        $messenger->addMessage(t('@count results processed.', ['@count' => count($results)]));
        // @todo enable only with verbose mode.
        // foreach ($results as $result) {
        //  $messenger->addMessage(print_r($result, TRUE));
        // }
      }
      else {
        $messenger->addMessage(t('No files to transliterate.'));
      }
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $messenger->addMessage(
        t('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

  /**
   * Sanitizes the filename.
   *
   * Port of Drupal\file\EventSubscriber\FileEventSubscriber::sanitizeFilename().
   *
   * @param string $filename_with_extension
   */
  public static function sanitizeFilename(string $filename_with_extension): string {
    $fileSettings = \Drupal::configFactory()->get('file.settings');
    $transliterate = $fileSettings->get('filename_sanitization.transliterate');

    $extension = pathinfo($filename_with_extension, PATHINFO_EXTENSION);
    if ($extension !== '') {
      $extension = '.' . $extension;
      $filename = pathinfo($filename_with_extension, PATHINFO_FILENAME);
    }

    // Sanitize the filename according to configuration.
    $alphanumeric = $fileSettings->get('filename_sanitization.replace_non_alphanumeric');
    $replacement = $fileSettings->get('filename_sanitization.replacement_character');
    if ($transliterate) {
      $transliterated_filename = \Drupal::service('transliteration')->transliterate(
        $filename,
        \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId(),
        $replacement
      );
      if (mb_strlen($transliterated_filename) > 0) {
        $filename = $transliterated_filename;
      }
      else {
        // If transliteration has resulted in a zero length string enable the
        // 'replace_non_alphanumeric' option and ignore the result of
        // transliteration.
        $alphanumeric = TRUE;
      }
    }
    if ($fileSettings->get('filename_sanitization.replace_whitespace')) {
      $filename = preg_replace('/\s/u', $replacement, trim($filename));
    }
    // Only honor replace_non_alphanumeric if transliterate is enabled.
    if ($transliterate && $alphanumeric) {
      $filename = preg_replace('/[^0-9A-Za-z_.-]/u', $replacement, $filename);
    }
    if ($fileSettings->get('filename_sanitization.deduplicate_separators')) {
      $filename = preg_replace('/(_)_+|(\.)\.+|(-)-+/u', $replacement, $filename);
      // Replace multiple separators with single one.
      $filename = preg_replace('/(_|\.|\-)[(_|\.|\-)]+/u', $replacement, $filename);
      $filename = preg_replace('/' . preg_quote($replacement) . '[' . preg_quote($replacement) . ']*/u', $replacement, $filename);
      // Remove replacement character from the end of the filename.
      $filename = rtrim($filename, $replacement);

      // If there is an extension remove dots from the end of the filename to
      // prevent duplicate dots.
      if (!empty($extension)) {
        $filename = rtrim($filename, '.');
      }
    }
    if ($fileSettings->get('filename_sanitization.lowercase')) {
      // Force lowercase to prevent issues on case-insensitive file systems.
      $filename = mb_strtolower($filename);
    }
    return $filename . $extension;
  }

}
