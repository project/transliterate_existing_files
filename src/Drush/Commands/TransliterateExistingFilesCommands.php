<?php

namespace Drupal\transliterate_existing_files\Drush\Commands;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\transliterate_existing_files\TransliterateExistingFiles;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drush commandfile.
 */
final class TransliterateExistingFilesCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Constructs a TransliterateExistingFilesCommands object.
   */
  public function __construct(
    private readonly TransliterateExistingFiles $transliterateExistingFiles,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('transliterate_existing_files'),
    );
  }

  /**
   * Apply the configured transliteration to existing files.
   */
  #[CLI\Command(name: 'transliterate:existing-files', aliases: ['tef'])]
  #[CLI\Option(name: 'dry-run', description: 'Check the effect of the command without actually applying it.')]
  #[CLI\Option(name: 'redirect', description: 'Create a redirect if the file name changes.')]
  #[CLI\Usage(name: 'transliterate:existing_files', description: 'Apply the configured transliteration to existing files.')]
  #[CLI\Usage(name: 'transliterate:existing_files --dry-run', description: 'Check the effect of the command without actually applying it.')]
  public function existingFiles($options = ['dry-run' => FALSE]) {
    $this->transliterateExistingFiles->batchTransliterate($options);
    // @todo add option to include / exclude file extensions.
    if ($options['dry-run']) {
      $this->logger()->notice($this->t('Executed in dry run mode. Changes will not be applied.'));
    }
  }

}
