## Transliterate Existing Files

Batch transliterate existing files.

Optionally create redirects for the file names that changed if
the [Redirect](https://www.drupal.org/project/redirect) module is installed

### Use cases

- Migration to another file system
- Transliteration rules changed or were just not enabled

### Configuration

Configure transliteration as desired in `/admin/config/media/file-system`
under `Sanitize filenames`.

### Drush usage

**It is strongly advised to have a backup of files and database before running this command.**

Check the effect of the command without actually applying it.

```bash
drush transliterate:existing-files --dry-run
```

Apply the configured transliteration to existing files.

```bash
drush transliterate:existing-files
```

Alias

```bash
drush tef
```

Optionally create redirects for the file names that changed.

```
drush transliterate:existing-files --redirect
```

### Requirements

- Drupal >= 10.2
- Drush >= 12

### Roadmap

- [ ] Handle verbose option to show detailed changes with Drush
- [ ] Add Drush option to include / exclude file extensions
- [x] Add Drush option to set the redirects to be created or not
- [ ] Add tests
